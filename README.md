# Hugo Full 360 Theme

[![Hugo](https://img.shields.io/badge/hugo-0.65-blue.svg)](https://gohugo.io)

### [Hugo](https://gohugo.io) theme

- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Shortcodes](#shortcodes)

## Features

- Applies updates to the [book](https://github.com/alex-shpak/hugo-book/) theme used in ElasticDW docs page
- Bootstrap Landkit Theme (May 29, 2020) for the EDW landing page

## Requirements

- Hugo 0.65 or higher
- Hugo extended version, read more [here](https://gohugo.io/news/0.48-relnotes/)

## Installation

Instructions on how to install hugo for each platform can be found in the following link: [Hugo - Getting Started](https://gohugo.io/getting-started/installing)

Navigate to your hugo project root and run:

For GitLab
```
git submodule add git@gitlab.full360.dev:vertica-saas/accelerator-hugo-theme.git themes/accelerator-hugo-theme
```

For BitBucket
```
git submodule add git@bitbucket.org:vertica-saas/accelerator-hugo-theme.git themes/accelerator-hugo-theme
```

Then run hugo (or set `theme = "accelerator-hugo-theme"`/`theme: accelerator-hugo-theme` in configuration file)

### Creating site from scratch

Below is example how to create new site from scratch

```sh
hugo new site mydocs; cd mydocs
git init
# If you're using Gitlab...
git submodule add git@gitlab.full360.dev:vertica-saas/accelerator-hugo-theme.git themes/accelerator-hugo-theme

# Or if you're using Bitbucket...
git submodule add git@bitbucket.org:vertica-saas/accelerator-hugo-theme.git themes/accelerator-hugo-theme
```

### Partials

Partials for Landkit

| Partial                                             | Purpose                  | Source |
| --------------------------------------------------- | ------------------------ | ------ |
| `layouts/partials/landkit/home/feature.html`        | Home Feature Sections    | Various - Landkit [Cloud](https://landkit.goodthemes.co/cloud.html) |
|                                                     |                          | and [Basic](https://landkit.goodthemes.co/index.html)            |
| `layouts/partials/landkit/home/hero.html`           | Home Hero Section        | Landkit [Basic](https://landkit.goodthemes.co/index.html)           |
| `layouts/partials/landkit/home/hero-cta.html`       | Home Hero CTA Section    | Landkit [Basic](https://landkit.goodthemes.co/index.html)           |
| `layouts/partials/landkit/home/modals.html`         | Home Modals Section      | Landkit [Cloud](https://landkit.goodthemes.co/cloud.html)           |
| `layouts/partials/landkit/home/solution.html`       | Home Solutions Section   | Landkit [Cloud](https://landkit.goodthemes.co/cloud.html)           |

## Shortcodes

 - [Swagger](layouts/shortcodes/swagger-spec.html)
 - [Table](layouts/shortcodes/table.html): A simple shortcode to specify css classes in the resulting html table of a markdown table.


 ### Image Sources
 Backgrounds etc. use https://pixabay.com/
 Logos and Diagrams are in Figma, the file is saved in the assets/.figma folder

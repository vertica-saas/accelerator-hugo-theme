var htt = require("html-to-text");

$(function () {
  var mediumPromise = new Promise(function (resolve) {
    var $content = $("#jsonContent");
    var data = {
      rss: "https://medium.com/feed/full360",
    };

    $.get(
      "https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2Ffull360",
      data,
      function (response) {
        if (response.status == "ok") {
          $("#logo").append(
            `<img src="${response.feed["image"]}" class="rounded mx-auto d-block">`
          );
          var display = "";
          var maxItems = 3;
          var items = response.items && response.items.slice(0, maxItems);
          $.each(items, function (k, item) {
            var src = item["thumbnail"]; // use thumbnail url
            var descriptionText = htt.fromString(item["description"], {
              ignoreImage: true,
              ignoreHref: true,
              wordwrap: false,
              singleNewLineParagraphs: true,
            });

            var maxLength = 150; // maximum number of characters to extract
            //trim the string to the maximum length
            var trimmedString = descriptionText.substr(0, maxLength);
            //re-trim if we are in the middle of a word
            trimmedString = trimmedString.substr(
              0,
              Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))
            );
            var pubDate = new Date(item.pubDate).toLocaleDateString("default", {
              month: "short",
              day: "numeric",
            });

            display += `<div class="col-12 col-lg-4 d-flex">`;
            display += `<div class="card mb-6 mb-lg-0 shadow-light-lg lift lift-lg">`;
            display += `<a class="card-img-top" href="${item.link}"><img src="${src}" alt="Medium Blog Image" class="card-img-top" style="height=100%"></a>`;
            display += `<div class="position-relative"><div class="shape shape-fluid-x shape-bottom svg-shim text-white"><svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 48h2880V0h-720C1442.5 52 720 0 720 0H0v48z" fill="currentColor"></path></svg></div></div>`;
            display += `<a class="card-body my-auto" href="${item.link}">`;
            display += `<h3 class="mt-auto">${item.title}</h3>`;
            display += `<p class="mb-0 text-muted">${trimmedString}...</p>`;
            display += `</a>`;
            display += `<a class="card-meta" href="${item.link}">`;
            display += `<hr class="card-meta-divider">`;
            // display += `<div class="avatar avatar-sm mr-2"><img src="assets/img/avatars/avatar-2.jpg" alt="..." class="avatar-img rounded-circle"></div>`
            display += `<h6 class="text-uppercase text-muted mr-2 mb-0">${item.author}</h6>`;
            display += `<p class="h6 text-uppercase text-muted mb-0 ml-auto"><time datetime="${item.pubDate}">${pubDate}</time></p>`;
            display += `</a>`;
            display += `</div>`;
            display += `</div>`;
          });

          resolve($content.html(display));
        }
      }
    );
  });

  mediumPromise.then(function () {
    //Pagination
    pageSize = 4;

    var pageCount = $(".card").length / pageSize;

    for (var i = 0; i < pageCount; i++) {
      $("#pagin").append(
        `<li class="page-item"><a class="page-link" href="#">${i + 1}</a></li> `
      );
    }
    $("#pagin li:nth-child(1)").addClass("active");
    showPage = function (page) {
      $(".card").hide();
      $(".card").each(function (n) {
        if (n >= pageSize * (page - 1) && n < pageSize * page) $(this).show();
      });
    };

    showPage(1);

    $("#pagin li").click(function () {
      $("#pagin li").removeClass("active");
      $(this).addClass("active");
      showPage(parseInt($(this).text()));
      return false;
    });
  });
});
